# Попробую сделать статическую переменную с помощью замыкания?

# Отличный новый факт про декораторы для меня -> если у декоратора есть аргумент, появляется еще одна "обертка"


def decorator_factory(var_name: str, number: int) -> not None:
    def decorator(func: object) -> not None:
        setattr(func, var_name, number)
        return func
    return decorator


@decorator_factory("counter", 0)
def foo() -> int:
    foo.counter += 1
    return foo.counter


print(foo())
print(foo())
