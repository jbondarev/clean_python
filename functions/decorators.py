def intro_decorator(fn):
    def intro(t):
        print("New costumer!")
        print(fn(t))
        print("Make comfortable!")

    return intro


@intro_decorator
def func(name):
    return f"Greetings, {name}!"


func("Yaroslav")


def great_view_decorator(fn):
    def decorate_fn(*args, **kwargs):
        print("This function can look better!")
        print(f"Also, you can see arguments for this function: ({args}, {kwargs})")
        print("Let's call it...")
        result = fn(*args, **kwargs)
        print(f"Result of the funtction {fn} is {result}")

    return decorate_fn


@great_view_decorator
def sum_(x, y):
    return x + y


sum_(5, 4)
