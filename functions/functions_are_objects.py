# Функции это объекты

def greetings(name):
    return f"Greetings, {name}!"


# Создадим другую ссылку на объект greetings

say_hello_func = greetings
print(greetings("Alexey"))
print(say_hello_func("Yaroslav"))

# Тем не менее greetings - тоже ссылка, ведь мы все еще можем использовать другую ссылку на объект
del greetings
print(say_hello_func("Igor"))

# greetings
print(say_hello_func.__name__)

# Функции могут храниться в структурах данных
from random import randint

fn_sum = lambda x, y: abs(x + y)
fn_dif = lambda x, y: abs(x - y)
fn_mul = lambda x, y: x * y
fn_div = lambda x, y: x / y if y != 0 else ZeroDivisionError
fn = [fn_sum, fn_dif, fn_mul, fn_div]
for i in range(len(fn)):
    x = randint(0, 100)
    y = randint(0, 100)
    print(f"{x}, {y}, {fn[i]}, {fn[i](x, y)}")


# Подвод к декораторам?

def get_speak_fn(volume):
    def whisper(text):
        return text.lower + "..."

    def scream(text):
        return text.upper + "!!!"

    if volume >= 0.5:
        return scream
    else:
        return whisper


print(get_speak_fn(0.5))
print(get_speak_fn(0))


# ?..

def make_adder(n):
    def add(x):
        return x + n

    return add


plus_3 = make_adder(3)
plus_5 = make_adder(5)
print(plus_3(4), plus_5(4), plus_3(plus_5(2)))


# Объекты могут вести себя как функции

class Adder(object):
    def __init__(self, x):
        self.x = x

    def __call__(self, x):
        return self.x + x


adder = Adder(3)
print(callable(Adder), callable(adder), callable(5))
print(adder(5))

# Функции, которые уносят часть состояния родительской функции называют замыканиями