a = [1, 2, 3]
b = a
print(a == b, a is b)
# True True

a = [1, 2, 3]
b = [1, 2, 3]

print(a == b, a is b)
# True False

a = [1, 2, 3]
b = list(a)

print(a == b, a is b)
# True False
