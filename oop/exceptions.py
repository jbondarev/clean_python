class NameTooShortError(ValueError):
    pass


def validate(name):
    if len(name) <= 5:
        raise NameTooShortError(name)


validate("Slava")
