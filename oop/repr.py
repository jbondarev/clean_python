class Car(object):
    def __init__(self, color, number):
        self.color = color
        self.number = number

    def __str__(self):
        return f"{self.color} автомобиль с номером: {self.number}"

    def __repr__(self):
        return f"{self.color} автомобиль с номером: {self.number}"


car = Car("синий", 12345)
print(car)
