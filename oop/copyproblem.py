import copy

# мелкая копия - дочерние элементы все еще изменяются
# глубокая копия - не изменяются

# Пример мелкой копии
print("Пример мелкой копии")

a = [[1, 2, 3], [2, 3, 4], [3, 4, 5]]
b = list(a)
print(a)
print(b)

print()

a.append("какая-то дичь")
print(a)
print(b)

a[1][1] = "__"
print(a)
print(b)

# Тоже самое с глубокой копией
print("Тоже самое с глубокой копией")
a = [[1, 2, 3], [2, 3, 4], [3, 4, 5]]
b = copy.deepcopy(a)

print(a)
print(b)

print()

a.append("какая-то дичь")
print(a)
print(b)

a[1][1] = "__"
print(a)
print(b)
