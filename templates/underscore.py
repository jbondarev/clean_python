# 1. Одинарный начальный символ подчеркивания _variable
# Является подсказкой для программиста, которая означает, что данная переменная имеет спецификатор доступа "private"
# Не обеспечивается интерпретатором Python


class Test(object):
    def __init__(self):
        self.foo = 1
        self._bar = 23


t = Test()
print(t.foo)
print(t._bar)

# Вывод - придерживаться рекомендаций PEP8, чтобы не получить еще больше проблем

# 2. Одинарный замыкающий символ подчеркивания variable_
# Костыль чтобы делать переменные по тиме def_, class_

def_ = 0
class_ = 0


# 3. Двойной начальный символ подчеркивания __variable
# Искажение переменной

class ManglingTest(object):
    def __init__(self):
        self.__text = "Hello, world!"

    def get_mangled(self):
        return self.__text


test = ManglingTest()
try:
    test.__text()
except Exception as e:
    print(e)
print(test.get_mangled())


# 4. Двойной начальный и замыкающий символ подчеркивания __variable__
# Используется для зарезервированных методов
# Можно использовать в своих программах, но зачем, если это может вызвать проблемы в будущем?

class Example(object):
    def __init__(self):
        pass

    def __abs__(self):
        pass

    def __call__(self, *args, **kwargs):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


# 5. Одинарный символ подчеркивания _
# Договоренность о незначительный элементах

a, _, c = (1, "useless", 2)
print(a, c)
print(_)