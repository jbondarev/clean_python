# Отличный вариант форматирования строк
a, b = 4, 5
string_ = "{0} + {1} = {2}".format(a, b, a + b)
print(string_)

# Интересный вариант форматирования строк (Python -V >= 3.6)
a, b = 5, 6
string_ = f"{a} + {b} = {a + b}"
print(string_)

# Интересный, безопасный, но не очень вариант

from string import Template
user = "Ярослав"
t = Template("Привет, $user!")
print(t.substitute(user=user))