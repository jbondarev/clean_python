def main():
    shoes = {"name": "Shoes", "price": 14900}
    print(apply_discount(shoes, 0.25))
    # Получим assertionError
    # print(apply_discount(shoes, 2.00))


# Функция assert будет гарантировать что цена товара не будет ниже нуля и выше цены изначального товара
# Assert нужно применять только в случае, когда мы ожидаем что assert-условие никогда не возникнет
def apply_discount(product, discount):
    price = int(product["price"] * (1.0 - discount))
    assert 0 <= price <= product["price"]
    return price


if __name__ == "__main__":
    main()

