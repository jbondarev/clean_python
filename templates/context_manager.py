import threading
import contextlib


# https://docs.python.org/3/library/stdtypes.html#typecontextmanager

class ManagedFile(object):
    def __init__(self, name):
        self.name = name

    # Вызывается когда поток исполнения входит в контекст иснетрукции with
    def __enter__(self):
        self.file = open(self.name, "w")
        return self.file

    # Вызывается на выходе контекста
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.file:
            self.file.close()


# Тоже самое можно переписать с помощью одной функции contextlib

@contextlib.contextmanager
def managed_file(name):
    try:
        f = open(name, "w")
        yield f
    finally:
        f.close()


class Indenter(object):
    def __init__(self):
        self.counter = 0

    def __enter__(self):
        self.counter += 1
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.counter -= 1

    def print(self, text=""):
        return "__" * self.counter + text


def main():
    with Indenter() as indent:
        print(indent.print("1-ый уровень"))
        # Тот же экземпляр класса
        with indent:
            print(indent.print("2-ой уровень"))
        # Выход из контекста
        print((indent.print("3-ий уровень")))


def example():
    some_lock = threading.Lock()
    # Вредно
    some_lock.acquire()
    try:
        pass
    finally:
        some_lock.release()

    # Полезно и удобно
    with some_lock:
        pass


if __name__ == "__main__":
    main()
